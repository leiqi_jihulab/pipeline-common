# Common CI for QA pipelines

[[_TOC_]]

This project has common CI config that is used by the gitlab-qa projects for different environments

## QA pipeline types

### Live environment pipelines

[README.md](ci/dot-com/README.md)

### Nightly pipelines

[README.md](ci/nightly/README.md)

## Release process

Changes to templates should be released in same fashion as any other dependency.

### Versioning

We follow [Semantic Versioning](https://semver.org).  In short, this means that the new version should reflect the types of changes that are about to be released.

*summary from semver.org*

MAJOR.MINOR.PATCH

- MAJOR version when you make backwards incompatible changes
- MINOR version when you add functionality in a backwards compatible manner
- PATCH version when you make backwards compatible bug fixes.

### When we release

We release `pipeline-common` on an ad-hoc basis.  There is no regularity to when we release, we just release
when we make a change - no matter the size of the change.

### How-to

Push a semver compatible tag with new version

### Changelog

It is recommended to use [Changelog](https://docs.gitlab.com/ee/development/changelog.html#overview) git trailers for marking commits for automated changelog entry generation

## Automated updates

[re-update.gitlab-ci.yml](ci/ref-update.gitlab-ci.yml) defines a job which will automatically check if a newer version of `pipeline-common`
is released and create merge request updating `ref` value in `.gitlab-ci.yml`.

Job requires following setup:

- `REF_UPDATE_GITLAB_API_TOKEN` environment variable with gitlab api token if outside `Quality Department` group
- [Schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) job with variable `UPDATE_INCLUDE_REF` set to `true`
- [Include](https://docs.gitlab.com/ee/ci/yaml/#includefile) definition with `ci/ref-update.gitlab-ci.yml`
- `update` [stage](https://docs.gitlab.com/ee/ci/yaml/#stages) present in ci configuration of the project receiving updates
