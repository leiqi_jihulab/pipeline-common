# dot-com

This section contains job definition for .com environment pipelines

## Projects

Following projects rely on these job definitions for test execution against `.com` environments:

* [staging-canary](https://gitlab.com/gitlab-org/quality/staging-canary)
* [staging](https://gitlab.com/gitlab-org/quality/staging)
* [staging-ref](https://gitlab.com/gitlab-org/quality/staging-ref)
* [preprod](https://gitlab.com/gitlab-org/quality/preprod)
* [canary](https://gitlab.com/gitlab-org/quality/canary)
* [production](https://gitlab.com/gitlab-org/quality/production)
* [release](https://gitlab.com/gitlab-org/quality/release)

Pipelines are executed on repository mirrors in ops instance: <https://ops.gitlab.net/gitlab-org/quality>

## Job definitions

### qa.gitlab-ci.yml

QA test job definitions

#### parallel test jobs

To make sure a job doesn't fetch a more recent knapsack report on a job retry, knapsack reports are downloaded in advance
and stored as artifacts.

In order for correct knapsack reports to be downloaded, a manual list of parallel jobs has to be maintained in
[.com:qa:rules:knapsack-report-download](../rules.gitlab-ci.yml) rule definition.

### cleanup.gitlab-ci.yml

Resource cleanup jobs:

* scheduled cleanup
* fabricated resource deletion right after test run

### reports.gitlab-ci.yml

Reporting job definition:

* allure test report
* test session report
* stage reports
